@extends('dealer.masterlayout')
<?php $title = "Product" ?>


@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Product
            <small>(Edit Product)</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('dealer/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><a href="{{ url('dealer/product') }}">Product</a></li>
            <li class="active">Edit Product</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- /.row -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Product</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    @if(Session::has('errors'))
                        <span class="help-block" style="color:red;margin-left: 10px;">
                            @if($errors->has('category'))
                                {{ $errors->first('category') }}
                            @elseif($errors->has('name'))
                                {{ $errors->first('name') }}
                            @elseif($errors->has('price'))
                                {{ $errors->first('price') }}
                            @elseif($errors->has('description'))
                                {{ $errors->first('description') }}
                            @endif
                        </span>
                    @endif
                    <form role="form" action="{{url('dealer/product/'.$product->id)}}" method="post" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="category_name">Select Category</label>
                                <select class="form-control category" name="category">
                                    @foreach($categories as $category)
                                        @if($category->id == $product->category_id)
                                            <option value="{{$category->id}}" selected>{{$category->name}}</option>
                                        @else
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <input name="_method" type="hidden" value="PATCH">
                                <label for="product_name">Enter Product Name</label>
                                <input type="text" class="form-control" id="product_name" placeholder="Enter the Product Name" name="product_name" value="{{$product->name}}">
                            </div>
                            <div class="form-group">
                                <label for="product_cost_price">Enter Product Cost Price</label>
                                <input type="text" class="form-control" id="product_cost_price" placeholder="Enter the Product Cost Price" name="product_cost_price" value="{{$product->cost_price}}">
                            </div>
                            <div class="form-group">
                                <label for="product_MRP">Enter Product MRP</label>
                                <input type="text" class="form-control" id="product_MRP" placeholder="Enter the Product MRP" name="product_MRP" value="{{$product->MRP}}">
                            </div>
                            <div class="form-group">
                                <label for="product_name">Enter Product Description</label>
                                <textarea id="product_description"  rows="10" cols="80" name="product_description">{{$product->description}}</textarea>

                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('custom_script')
<script>
    $(document).ready(function()
    {
        CKEDITOR.replace('product_description');
    });
</script>
@endsection

