@extends('dealer.masterlayout')
<?php $title = "Category" ?>


@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Category
            <small>(Add new Category)</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('dealer/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><a href="{{ url('dealer/category') }}">Category</a></li>
            <li class="active">Add Category</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- /.row -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add new Category</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    @if(Session::has('errors'))
                        <span class="help-block" style="color:red;margin-left: 10px;">
                            @if($errors->has('category'))
                                {{ $errors->first('category') }}
                            @elseif($errors->has('image'))
                                {{ $errors->first('image') }}
                            @endif
                        </span>
                    @endif
                    <form role="form" action="{{url('dealer/category')}}" method="post" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="category">Enter Category Name</label>
                                <input type="text" class="form-control" id="category" placeholder="Enter the Category Name" name="category_name">
                            </div>
                            <div class="form-group">
                                <label for="category_image">Enter Category Image</label>
                                <input type="file" id="category_image" name="image">
                            </div>
                            <div class="form-group">
                                <label for="category_tag">Enter Category Tag</label>
                                <input type="text" class="form-control" id="category_tag" placeholder="Enter the Category Tag" name="category_tag">
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </section>
@endsection

