@extends('dealer.masterlayout')
<?php $title = "Product"?>

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Product
            <small>(View, Add, Edit product)</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('dealer/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Product</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        @if(Session::has('success'))
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {{ Session::get('success') }}
                            </div>
                        @endif
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <a href="{{ url('dealer/product/create') }}" class="btn  btn-primary btn-flat" style="position: absolute;left:10px">Add New product</a>
                        <div style="clear:right"></div>
                        <table id="data-table" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Sl No.</th>
                                <th>Category Name</th>
                                <th>Product Name</th>
                                <th>Product Cost Price</th>
                                <th>Product MRP</th>
                                <th>Product Description</th>
                                <th>Update Product</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i=0 ?>
                            @foreach($products as $product)
                                <tr>
                                    <td>{{$i+1}}</td>
                                    <td>{{$category_name[$i]}}</td>
                                    <td>{{$product->name}}</td>
                                    <td>{{$product->cost_price}}</td>
                                    <td>{{$product->MRP}}</td>
                                    <td>{!! $product->description !!}</td>
                                    <td><a href="{{ url('dealer/product/'.$product->id.'/edit') }}"><i class="fa fa-edit"></i>  Edit</a></td>
                                </tr>
                                <?php $i++ ?>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('custom_script')
    <script>
        $(function ()
        {
            $('#data-table').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
@endsection


