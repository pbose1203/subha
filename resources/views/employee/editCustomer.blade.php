@extends('employee.masterlayout')
<?php $title = "Customer" ?>

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Customer
            <small>(View, Add, Edit dealer)</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('employee/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Customer</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- /.row -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add new User</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    @if(Session::has('errors'))
                        <span class="help-block" style="color:red;margin-left: 10px;">
                            @if($errors->has('category'))
                                {{ $errors->first('category') }}
                            @elseif($errors->has('image'))
                                {{ $errors->first('image') }}
                            @endif
                        </span>
                    @endif

                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-6 col-6">
                                <div class="verify-name">Name:</div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-6">
                                <div class="verify-content"> {{$customer->name}} </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-2">
                                <a href="{{url('employee/customer/edit/'.$customer->id)}}" class="btn">Edit</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-6 col-6">
                                <div class="verify-name">Mobile:</div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-6">
                                <div class="verify-content"> {{$customer->mobile}} </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 col-md-4 col-sm-6 col-6">
                                <div class="verify-name">Current Balance:</div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-6">
                                <div class="verify-content"> {{$customer->balance}} </div>
                            </div>
                        </div>
                    </div>
                    <form class="check-mobile-form" method="post" action="{{url('employee/customer/edit')}}">
                        {!! csrf_field() !!}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="category">Enter Name:</label>
                                <input type="hidden" value="{{$customer->id}}" name="id">
                                <input type="text" class="form-control mobile" id="category" placeholder="Enter Name" name="name" value="{{$customer->name}}" >
                            </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button class="btn btn-primary check-mobile-btn" data-target="#verify_code">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </section>
@endsection




