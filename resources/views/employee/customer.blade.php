@extends('employee.masterlayout')
<?php $title = "Customer" ?>

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Customer
            <small>(View, Add, Edit dealer)</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('employee/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Customer</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- /.row -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add new Customer</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    @if(Session::has('errors'))
                        <span class="help-block" style="color:red;margin-left: 10px;">
                            @if($errors->has('category'))
                                {{ $errors->first('category') }}
                            @elseif($errors->has('image'))
                                {{ $errors->first('image') }}
                            @endif
                        </span>
                    @endif
                    <form class="check-mobile-form" method="post" action="{{url('employee/check_customer')}}">
                        {!! csrf_field() !!}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="category">Enter Phone No:</label>
                                <input type="text" class="form-control mobile" id="category" placeholder="Enter phone no" name="phone_no" {{--onblur="checkCustomer()"--}}>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button class="btn btn-primary check-mobile-btn" data-target="#verify_code">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
            <!--  modal open -->
            {{--<div class="modal fade" id="verify_code" role="dialog" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Customer Details</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-8 user-informations">
                                    --}}{{--<div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                            <div class="verify-name">Name:</div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                            <div class="verify-content"> @if (!empty($customer)) hell @endif </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                            <div class="verify-name">Mobile No:</div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                            <div class="verify-content"> @if (!empty($customer)) hello @endif </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                            <div class="verify-name">Credit Balance:</div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                            <div class="verify-content"></div>
                                        </div>
                                    </div>--}}{{--
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>--}}

            <!-- Message Modal -->
            {{--<div class="modal fade" id="message" role="dialog" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Message</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="message-content"></div>
                        </div>
                    </div>
                </div>
            </div>--}}
            <!-- modal close -->
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('custom_script')



@endsection


