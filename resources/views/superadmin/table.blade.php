@extends('superadmin.masterlayout')
<?php $title = "Table" ?>

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Waiters
            <small>(View, Add, Edit Table)</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('superadmin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Table</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        @if(Session::has('success'))
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {{ Session::get('success') }}
                            </div>
                        @endif
                            @if(Session::has('error'))
                                <div class="alert alert-error alert-dismissable">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    {{ Session::get('error') }}
                                </div>
                            @endif
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <a href="{{ url('superadmin/table/create') }}" class="btn  btn-primary btn-flat" style="position: absolute;left:10px">Add New Table</a>
                        <div style="clear:right"></div>
                        <table id="data-table" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Sl No.</th>
                                <th>Table Number</th>
                                <th>Status</th>
                                {{--<th>Edit</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i=0 ?>
                            @foreach($tables as $table)
                                <tr>
                                    <td>{{$i+1}}</td>
                                    <td>{{$table->table_no}}</td>
                                    <td>@if($table->active == 0 ) Available @else Reserved @endif</td>
                                    <!--<td><a href="{{ url('superadmin/table/'.$table->id.'/edit') }}"><i class="fa fa-edit"></i>  Edit</a></td>-->
                                </tr>
                                <?php $i++ ?>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>

@endsection

@section('custom_script')

    <script>
        $(function () {
            $('#data-table').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>

@endsection


