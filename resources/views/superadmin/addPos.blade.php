@extends('superadmin.masterlayout')
<?php $title = "Pos" ?>


@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Pos
            <small>(Add new Dealer)</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('superadmin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><a href="{{ url('superadmin/Pos') }}">Pos</a></li>
            <li class="active">Add Employee</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- /.row -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add new Pos</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    @if(Session::has('errors'))
                        <span class="help-block" style="color:red;margin-left: 10px;">
                            @if($errors->has('type'))
                                {{ $errors->first('type') }}
                            @elseif($errors->has('question'))
                                {{ $errors->first('question') }}
                            @elseif($errors->has('answer'))
                                {{ $errors->first('answer') }}
                            @endif
                        </span>
                    @endif
                    <form role="form" action="{{url('superadmin/pos')}}" method="post">
                        {!! csrf_field() !!}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Enter the Pos Name</label>
                                <input type="text" class="form-control" id="name" placeholder="Enter the Pos Name" name="name" >
                            </div>
                            <div class="form-group">
                                <label for="email">Enter the Pos Display Name</label>
                                <input type="text" class="form-control" id="email" placeholder="Enter the Pos Display Name" name="display_name" >
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </section>
@endsection

