@extends('superadmin.masterlayout')
<?php $title = "Master" ?>

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dealer
            <small>(View, Add, Edit dealer)</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('superadmin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Dealer</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        @if(Session::has('success'))
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {{ Session::get('success') }}
                            </div>
                        @endif
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <a href="{{ url('superadmin/master/create') }}" class="btn  btn-primary btn-flat" style="position: absolute;left:10px">Add New Master</a>
                        <div style="clear:right"></div>
                        <table id="data-table" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Sl No.</th>
                                <th>Master Name</th>
                                <th>Master Email</th>
                                <th>Master Phone</th>
                                <th>Master Address</th>
                                <th>Member Since</th>
                                <th>Update Master</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i=0 ?>
                            @foreach($dealers as $dealer)
                                <tr>
                                    <td>{{$i+1}}</td>
                                    <td>{{$dealer->name}}</td>
                                    <td>{{$dealer->email}}</td>
                                    <td>{{$dealer->phone}}</td>
                                    <td>{{$dealer->address}}</td>
                                    <td>{{date('d/m/y',strtotime($dealer->created_at))}}</td>
                                    <td><a href="{{ url('superadmin/master/'.$dealer->id.'/edit') }}"><i class="fa fa-edit"></i>  Edit</a></td>
                                </tr>
                                <?php $i++ ?>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>

@endsection

@section('custom_script')

    <script>
        $(function () {
            $('#data-table').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>

@endsection


