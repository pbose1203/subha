@extends('superadmin.masterlayout')
<?php $title = "Dealer" ?>


@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dealer
            <small>(Add new Dealer)</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('superadmin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><a href="{{ url('superadmin/dealer') }}">Dealer</a></li>
            <li class="active">Add Dealer</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- /.row -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add new Dealer</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    @if(Session::has('errors'))
                        <span class="help-block" style="color:red;margin-left: 10px;">
                            @if($errors->has('type'))
                                {{ $errors->first('type') }}
                            @elseif($errors->has('question'))
                                {{ $errors->first('question') }}
                            @elseif($errors->has('answer'))
                                {{ $errors->first('answer') }}
                            @endif
                        </span>
                    @endif
                    <form role="form" action="{{url('superadmin/dealer')}}" method="post">
                        {!! csrf_field() !!}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Enter Dealer Name</label>
                                <input type="text" class="form-control" id="name" placeholder="Enter the Dealer Name" name="dealer_name" >
                            </div>
                            <div class="form-group">
                                <label for="email">Enter Dealer Email</label>
                                <input type="text" class="form-control" id="email" placeholder="Enter the Dealer Email" name="dealer_email" >
                            </div>
                            <div class="form-group">
                                <label for="phone">Enter Dealer Phone No.</label>
                                <input type="text" class="form-control" id="phone" placeholder="Enter the Dealer Number" name="dealer_phone" >
                            </div>
                            <div class="form-group">
                                <label for="address">Enter Dealer Address</label>
                                <input type="text" class="form-control" id="address" placeholder="Enter the Dealer Address" name="dealer_address" >
                            </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </section>
@endsection

