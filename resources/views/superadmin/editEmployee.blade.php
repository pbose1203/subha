@extends('superadmin.masterlayout')
<?php $title = "Employee" ?>


@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Employee
            <small>(Edit Dealer)</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('superadmin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><a href="{{ url('superadmin/employee') }}">Employee</a></li>
            <li class="active">Edit Employee</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- /.row -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Employee</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    @if(Session::has('errors'))
                        <span class="help-block" style="color:red;margin-left: 10px;">
                            @if($errors->has('type'))
                                {{ $errors->first('type') }}
                            @elseif($errors->has('question'))
                                {{ $errors->first('question') }}
                            @elseif($errors->has('answer'))
                                {{ $errors->first('answer') }}
                            @endif
                        </span>
                    @endif
                    <form role="form" action="{{url('superadmin/employee/'.$employee->id)}}" method="post">
                        {!! csrf_field() !!}
                        <div class="box-body">
                            <div class="form-group">
                                <input name="_method" type="hidden" value="PATCH">
                                <label for="name">Enter Dealer Name</label>
                                <input type="text" class="form-control" id="name" placeholder="Enter the Employee Name" name="dealer_name" value="{{$employee->name}}">
                            </div>
                            <div class="form-group">
                                <label for="email">Enter Dealer Email</label>
                                <input type="text" class="form-control" id="email" placeholder="Enter the Employee Email" name="dealer_email" value="{{$employee->email}}">
                            </div>
                            <div class="form-group">
                                <label for="phone">Enter Dealer Phone No.</label>
                                <input type="text" class="form-control" id="phone" placeholder="Enter the Employee Number" name="dealer_phone" value="{{$employee->phone}}">
                            </div>
                            <div class="form-group">
                                <label for="address">Enter Dealer Address</label>
                                <input type="text" class="form-control" id="address" placeholder="Enter the Employee Address" name="dealer_address" value="{{$employee->address}}">
                            </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </section>
@endsection

