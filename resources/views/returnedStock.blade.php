@extends('dealer.masterlayout')
<?php $title = "Returned Stock"?>

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Returned Stock
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('dealer/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><a href="#">Returned Stock</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- /.row -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Returned Stock</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    @if(Session::has('errors'))
                        <span class="help-block" style="color:red;margin-left: 10px;">
                            @if($errors->has('type'))
                                {{ $errors->first('type') }}
                            @elseif($errors->has('question'))
                                {{ $errors->first('question') }}
                            @elseif($errors->has('answer'))
                                {{ $errors->first('answer') }}
                            @endif
                        </span>
                    @endif
                    {!! csrf_field() !!}
                    <div class="box-body">
                        <form class="stock_form" role="form"  method="post" >
                            <div class="form-group">
                                <input type="text" class="form-control invoice_no" name="invoice" placeholder="Enter Invoice Number" onblur="productdetails()">
                            </div>
                        </form>
                        <div class="product-container">
                            <div class="product-header">

                            </div>
                            <div class="product-list">

                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('custom_script')
<script>
    function productdetails()
    {
        var invoice = $('.invoice_no').val();
        $.ajax(
        {
            type : 'GET',
            url  : '{{url('dealer/check_invoice')}}',
            data : {invoice:invoice},
            success : function(response)
            {
                var json = $.parseJSON(response);
                for (var i = 0; i < json.length ; i++)
                {
                    $('.product-header').append("<div><span class='bold'>Customer Name:</span> "+json[i].customer_name+"</div><div><span class='bold'>Customer Phone:</span> "+json[i].customer_phone+"</div><div><span class='bold'>Payment Date:</span> "+json[i].payment_date+"</div><div><span class='bold'>Status: </span>"+json[i].status+"</div>");

                    if(json[i].status=="Return option available")
                    {
                        $('.product-list').append("<div class='list "+ json[i].stock_id+"'><input type='checkbox' class='stock' name='stock[]' checked value='"+ json[i].stock+"' style='margin-right: 20px'><div class='inline'><span class='bold'>Product Name: </span>"+json[i].name +"</div><div class='inline'><span class='bold'>Product Price: </span>"+json[i].price+"</div></div>");
                    }
                    else
                    {
                        $('.product-list').append("<div class='list "+ json[i].stock_id+"'><div class='inline'><span class='bold'>Product Name: </span>"+ json[i].name +"</div><div class='inline'><span class='bold'>Product Price: </span>"+ json[i].price+"</div></div>");
                    }
                }
            },
            error:function(response)
            {
                alert("Invalid Invoice Number");
            }

        });

        $('.invoice_no').val("");
        $('.product-header').html("");
        $('.product-list').html("");

    }

</script>
@endsection