<?php $title = "Product"?>

<?php $__env->startSection('content'); ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Product
            <small>(View, Add, Edit product)</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo e(url('superadmin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Product</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <?php if(Session::has('success')): ?>
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <?php echo e(Session::get('success')); ?>

                            </div>
                        <?php endif; ?>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <a href="<?php echo e(url('superadmin/product/create')); ?>" class="btn  btn-primary btn-flat" style="position: absolute;left:10px">Add New product</a>
                        <div style="clear:right"></div>
                        <table id="data-table" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Sl No.</th>
                                <th>Category Name</th>
                                <th>Product Name</th>
                                <th>Product Cost Price</th>
                                <th>Product MRP</th>
                                <th>Product Description</th>
                                <th>Update Product</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i=0 ?>
                            <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($i+1); ?></td>
                                    <td><?php echo e($category_name[$i]); ?></td>
                                    <td><?php echo e($product->name); ?></td>
                                    <td><?php echo e($product->cost_price); ?></td>
                                    <td><?php echo e($product->MRP); ?></td>
                                    <td><?php echo $product->description; ?></td>
                                    <td><a href="<?php echo e(url('superadmin/product/'.$product->id.'/edit')); ?>"><i class="fa fa-edit"></i>  Edit</a></td>
                                </tr>
                                <?php $i++ ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom_script'); ?>
    <script>
        $(function ()
        {
            $('#data-table').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
<?php $__env->stopSection(); ?>



<?php echo $__env->make('superadmin.masterlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>