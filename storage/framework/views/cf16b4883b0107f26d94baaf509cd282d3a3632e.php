<?php $title = "Product" ?>


<?php $__env->startSection('content'); ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Product
            <small>(Edit Product)</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo e(url('superadmin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><a href="<?php echo e(url('superadmin/product')); ?>">Product</a></li>
            <li class="active">Edit Product</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- /.row -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Product</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <?php if(Session::has('errors')): ?>
                        <span class="help-block" style="color:red;margin-left: 10px;">
                            <?php if($errors->has('category')): ?>
                                <?php echo e($errors->first('category')); ?>

                            <?php elseif($errors->has('name')): ?>
                                <?php echo e($errors->first('name')); ?>

                            <?php elseif($errors->has('price')): ?>
                                <?php echo e($errors->first('price')); ?>

                            <?php elseif($errors->has('description')): ?>
                                <?php echo e($errors->first('description')); ?>

                            <?php endif; ?>
                        </span>
                    <?php endif; ?>
                    <form role="form" action="<?php echo e(url('superadmin/product/'.$product->id)); ?>" method="post" enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="category_name">Select Category</label>
                                <select class="form-control category" name="category">
                                    <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($category->id == $product->category_id): ?>
                                            <option value="<?php echo e($category->id); ?>" selected><?php echo e($category->name); ?></option>
                                        <?php else: ?>
                                            <option value="<?php echo e($category->id); ?>"><?php echo e($category->name); ?></option>
                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <input name="_method" type="hidden" value="PATCH">
                                <label for="product_name">Enter Product Name</label>
                                <input type="text" class="form-control" id="product_name" placeholder="Enter the Product Name" name="product_name" value="<?php echo e($product->name); ?>">
                            </div>
                            <div class="form-group">
                                <label for="product_cost_price">Enter Product Cost Price</label>
                                <input type="text" class="form-control" id="product_cost_price" placeholder="Enter the Product Cost Price" name="product_cost_price" value="<?php echo e($product->cost_price); ?>">
                            </div>
                            <div class="form-group">
                                <label for="product_MRP">Enter Product MRP</label>
                                <input type="text" class="form-control" id="product_MRP" placeholder="Enter the Product MRP" name="product_MRP" value="<?php echo e($product->MRP); ?>">
                            </div>
                            <div class="form-group">
                                <label for="product_name">Enter Product Description</label>
                                <textarea id="product_description"  rows="10" cols="80" name="product_description"><?php echo e($product->description); ?></textarea>

                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom_script'); ?>
<script>
    $(document).ready(function()
    {
        CKEDITOR.replace('product_description');
    });
</script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('superadmin.masterlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>