<?php $title = "Waiter" ?>


<?php $__env->startSection('content'); ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Pos
            <small>(Add new Dealer)</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo e(url('superadmin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><a href="<?php echo e(url('superadmin/Pos')); ?>">Pos</a></li>
            <li class="active">Add Employee</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- /.row -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add new Pos</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <?php if(Session::has('errors')): ?>
                        <span class="help-block" style="color:red;margin-left: 10px;">
                            <?php if($errors->has('type')): ?>
                                <?php echo e($errors->first('type')); ?>

                            <?php elseif($errors->has('question')): ?>
                                <?php echo e($errors->first('question')); ?>

                            <?php elseif($errors->has('answer')): ?>
                                <?php echo e($errors->first('answer')); ?>

                            <?php endif; ?>
                        </span>
                    <?php endif; ?>
                    <form role="form" action="<?php echo e(url('superadmin/waiter')); ?>" method="post">
                        <?php echo csrf_field(); ?>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Enter the Waiter Name</label>
                                <input type="text" class="form-control" id="name" placeholder="Enter the Waiter Name" name="name" >
                            </div>
                            <div class="form-group">
                                <label for="email">Enter the Waiter Sort Name</label>
                                <input type="text" class="form-control" id="email" placeholder="Enter the Waiter Sort Name" name="sort_name" >
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </section>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('superadmin.masterlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>