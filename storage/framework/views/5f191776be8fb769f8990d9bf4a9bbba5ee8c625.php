
<?php $title = "Customer" ?>

<?php $__env->startSection('content'); ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Customer
            <small>(View, Add, Edit dealer)</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo e(url('employee/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Customer</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- /.row -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add new User</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <?php if(Session::has('errors')): ?>
                        <span class="help-block" style="color:red;margin-left: 10px;">
                            <?php if($errors->has('category')): ?>
                                <?php echo e($errors->first('category')); ?>

                            <?php elseif($errors->has('image')): ?>
                                <?php echo e($errors->first('image')); ?>

                            <?php endif; ?>
                        </span>
                    <?php endif; ?>

                    <div class="box-body">
                        <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-6 col-6">
                            <div class="verify-name">Name:</div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-6">
                            <div class="verify-content"> <?php echo e($customer->name); ?> </div>
                        </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-2">
                                <a href="<?php echo e(url('employee/customer/edit/'.$customer->id)); ?>" class="btn">Edit</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-6 col-6">
                                <div class="verify-name">Mobile:</div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-6">
                                <div class="verify-content"> <?php echo e($customer->mobile); ?> </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 col-md-4 col-sm-6 col-6">
                                <div class="verify-name">Current Balance:</div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-6">
                                <div class="verify-content"> <?php echo e($customer->balance); ?> </div>
                            </div>
                        </div>
                    </div>
                    <form class="check-mobile-form" method="post" action="<?php echo e(url('employee/add_money')); ?>">
                        <?php echo csrf_field(); ?>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="category">Enter Amount:</label>
                                <input type="hidden" value="<?php echo e($customer->id); ?>" name="id">
                                <input type="text" class="form-control mobile" id="category" placeholder="Enter phone no" name="balance" >
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button class="btn btn-primary check-mobile-btn" data-target="#verify_code">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </section>
<?php $__env->stopSection(); ?>





<?php echo $__env->make('employee.masterlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>