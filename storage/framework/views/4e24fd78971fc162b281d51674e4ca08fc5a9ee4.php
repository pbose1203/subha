<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>TPL | Employee</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link href="<?php echo e(url('public/bootstrap/css/bootstrap.min.css')); ?>" rel="stylesheet" type="text/css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link href="<?php echo e(url('public/dist/css/AdminLTE.css')); ?>" rel="stylesheet" type="text/css">

</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <!--<img src="<?php echo e(url('public/dist/img/logo.png')); ?>" alt="logo">-->
      <p>TPL</p>
  </div>
  <div class="login-box-body">
    <p class="login-box-msg" style="font-size: 22px">Employee</p>
    <form action="<?php echo e(url('employee/authenticate')); ?>" method="post">

      <?php echo csrf_field(); ?>


      <div class="form-group">
        <div class="input-group">
          <div class="input-group-addon">
            <i class="fa fa-user"></i>
          </div>
          <input type="email" class="form-control pull-right" name="email">
        </div>
      </div>

      <div class="form-group">
        <div class="input-group">
          <div class="input-group-addon">
            <i class="fa fa-lock"></i>
          </div>
          <input type="password" class="form-control pull-right" name="password">
        </div>
      </div>
      <?php if(Session::has('error')): ?>
        <span class="help-block">
            <?php echo e(Session::get('error')); ?>

          </span>
      <?php endif; ?>
      <div class="row">
        <div class="col-xs-7 ">
          <label>
            <input type="checkbox"> Remember Me
          </label>
        </div>
        <div class="col-xs-5">
          <a href="#" data-toggle="modal" data-target="#forgot_password">Forgot password?</a><br>
        </div>
      </div>
      <div>
        <button type="submit" class="btn btn-primary btn-block btn-flat"  style="margin: 20px auto;">SIGN IN</button>
      </div>
    </form>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<div id="forgot_password" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Reset Password</h4>
      </div>
      <div class="modal-body">
        <form >
          <?php echo csrf_field(); ?>

          <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">
                <i class="fa fa-envelope"></i>
              </div>
              <input type="email" class="form-control forgot-email" placeholder="Enter registered Email" />
            </div>
          </div>
          <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">
                <i class="fa fa-phone"></i>
              </div>
              <input type="text" class="form-control forgot-mobile" placeholder="Enter registered Mobile" />
            </div>
          </div>
          <div class="error_message" ></div>
          <div class="modal-footer">
            <input type="submit" class="forgotpassword" value="Reset Password"/>
          </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
</div>

<!-- jQuery 2.2.3 -->
<script src="<?php echo e(url('public/plugins/jQuery/jquery-2.2.3.min.js')); ?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo e(url('public/bootstrap/js/bootstrap.min.js')); ?>"></script>
<script>
  $('.forgotpassword').click(function(e)
  {
    e.preventDefault();
    var email = $('.forgot-email').val();
    var mobile = $('.forgot-mobile').val();
    if(email=="")
    {
      $(".error_message").html("<p style='color:red'>Please enter registered email </p>");
    }
    else if(mobile=="")
    {
      $(".error_message").html("<p style='color:red'>Please enter registered mobile </p>");
    }
    else
    {
      $.ajax(
      {
        type: 'GET',
        url: '<?php echo e(url('forgotpassword')); ?>',
        data: {email: email,mobile:mobile},
        success: function (response)
        {
          if ($.trim(response)=="success")
          {
            $(".error_message").html("<p style='color:green'>Password updated Successfully check your email</p>");
            $('.forgotpassword').css('display','none');
          }
          else
          {
            $(".error_message").html("<p style='color:red'>" + response + "</p>");
          }

        }

      });
    }
  });
</script>
</body>
</html>
