<?php

    use App\Category;
    use App\Product;
    use App\User;
    $categories = Category::count();
    $products = Product::count();
    $employees = User::where('type','employee')->count();

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>TPL | <?php echo e($title); ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link href="<?php echo e(url('public/bootstrap/css/bootstrap.min.css')); ?>" rel="stylesheet" type="text/css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Theme style -->
    <link href="<?php echo e(url('public/dist/css/AdminLTE.css')); ?>" rel="stylesheet" type="text/css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins folder -->
    <link href="<?php echo e(url('public/dist/css/skins/skin-blue.min.css')); ?>" rel="stylesheet" type="text/css">
    <!-- Date Picker -->
    <link href="<?php echo e(url('public/plugins/datepicker/datepicker3.css')); ?>" rel="stylesheet" type="text/css">
    <!-- Time Picker -->
    <link href="<?php echo e(url('public/plugins/timepicker/bootstrap-timepicker.min.css')); ?>" rel="stylesheet" type="text/css">
    <!-- Data Table -->
    <link href="<?php echo e(url('public/plugins/datatables/dataTables.bootstrap.css')); ?>" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <header class="main-header">
        <!-- Logo -->
        <a href="<?php echo e(url('dealer/dashboard')); ?>" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini">TPL</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg">TPL</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    <!-- Notifications: style can be found in dropdown.less -->
                    <li class="dropdown notifications-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell-o"></i>
                            <span class="label label-warning notification"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">You have <span class="notification"></span> notification</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu">
                                    <li class="order" style="display:none"></li>
                                </ul>
                            </li>
                        </ul>
                    </li>

                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?php echo e(url('public/dist/img/user2-160x160.jpg')); ?>" class="user-image" alt="User Image">
                            <span class="hidden-xs"><?php echo e(Auth::user()->name); ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- Dealer image -->
                            <li class="user-header">
                                <img src="<?php echo e(url('public/dist/img/user2-160x160.jpg')); ?>" class="img-circle" alt="User Image">

                                <p>
                                    <?php echo e(Auth::user()->name); ?>

                                    <small>Member since  <?php echo e(date('F Y',strtotime(Auth::user()->created_at))); ?></small>
                                </p>
                                <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="#" class="btn btn-default btn-flat" data-toggle="modal" data-target="#password">Change Password</a>
                                </div>
                                <div class="pull-right">
                                    <a href="<?php echo e(url('dealer/logout')); ?>" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?php echo e(url('public/dist/img/user2-160x160.jpg')); ?>" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p><?php echo e(Auth::user()->name); ?></p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- search form -->
            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
                </div>
            </form>
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <?php if($title=="Dashboard"): ?>
                    <li class="active">
                        <a href="<?php echo e(url('dealer/dashboard')); ?>">
                            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                        </a>
                    </li>
                <?php else: ?>
                    <li class="">
                        <a href="<?php echo e(url('dealer/dashboard')); ?>">
                            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                        </a>
                    </li>
                <?php endif; ?>
                    <?php if($title=="Category"): ?>
                        <li class="active">
                            <a href="<?php echo e(url('dealer/category')); ?>">
                                <i class="fa fa-server"></i> <span>Category</span>
                                <span class="pull-right-container">
                                <span class="label label-primary bg-orange pull-right"><?php echo e($categories); ?></span>
                            </span>
                            </a>
                        </li>
                    <?php else: ?>
                        <li class="">
                            <a href="<?php echo e(url('dealer/category')); ?>">
                                <i class="fa fa-server"></i> <span>Category</span>
                                <span class="pull-right-container">
                                <span class="label label-primary bg-orange pull-right"><?php echo e($categories); ?></span>
                            </span>
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php if($title=="Product"): ?>
                        <li class="active">
                            <a href="<?php echo e(url('dealer/product')); ?>">
                                <i class="fa fa-shopping-cart"></i> <span>Product</span>
                                <span class="pull-right-container">
                                <span class="label label-primary bg-aqua pull-right"><?php echo e($products); ?></span>
                            </span>
                            </a>
                        </li>
                    <?php else: ?>
                        <li class="">
                            <a href="<?php echo e(url('dealer/product')); ?>">
                                <i class="fa fa-shopping-cart"></i> <span>Product</span>
                                <span class="pull-right-container">
                                <span class="label label-primary bg-aqua pull-right"><?php echo e($products); ?></span>
                            </span>
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php if($title=="Employee"): ?>
                        <li class="active">
                            <a href="<?php echo e(url('dealer/employee')); ?>">
                                <i class="fa fa-shopping-cart"></i> <span>Employee</span>
                                <span class="pull-right-container">
                                <span class="label label-primary bg-aqua pull-right"><?php echo e($employees); ?></span>
                            </span>
                            </a>
                        </li>
                    <?php else: ?>
                        <li class="">
                            <a href="<?php echo e(url('dealer/employee')); ?>">
                                <i class="fa fa-shopping-cart"></i> <span>Employee</span>
                                <span class="pull-right-container">
                                <span class="label label-primary bg-aqua pull-right"><?php echo e($employees); ?></span>
                            </span>
                            </a>
                        </li>
                <?php endif; ?>


            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    <?php echo $__env->yieldContent('content'); ?>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer" style="text-align: center">
        <strong>Copyright &copy; 2017 <a href="#" target="_blank"> TPL</a>.</strong> All rights
        reserved.
    </footer>
    <div id="password" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Change Password</h4>
                </div>
                <div class="modal-body">
                    <form action="<?php echo e(url('dealer/changepassword')); ?>" method="post">
                        <?php echo csrf_field(); ?>

                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-lock"></i>
                                </div>
                                <input type="text" class="form-control pull-right old_pass" name="old_pass" placeholder="Enter Old Password">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-lock"></i>
                                </div>
                                <input type="text" class="form-control pull-right new_pass" name="new_pass" placeholder="Enter New Password">
                            </div>
                        </div>
                        <div class="error_message" ></div>
                        <div class="modal-footer">
                            <input type="submit" class="btn btn-primary update_password" value="Update Password"/>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo e(url('public/plugins/jQuery/jquery-2.2.3.min.js')); ?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>$.widget.bridge('uibutton', $.ui.button);</script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo e(url('public/bootstrap/js/bootstrap.min.js')); ?>"></script>
<!-- Sparkline -->
<script src="<?php echo e(url('public/plugins/sparkline/jquery.sparkline.min.js')); ?>"></script>
<!-- Data-table -->
<script src="<?php echo e(url('public/plugins/datatables/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(url('public/plugins/datatables/dataTables.bootstrap.min.js')); ?>"></script>
<!-- datepicker -->
<script src="<?php echo e(url('public/plugins/datepicker/bootstrap-datepicker.js')); ?>"></script>
<!-- bootstrap time picker -->
<script src="<?php echo e(url('public/plugins/timepicker/bootstrap-timepicker.min.js')); ?>"></script>
<!-- Slimscroll -->
<script src="<?php echo e(url('public/plugins/slimScroll/jquery.slimscroll.min.js')); ?>"></script>
<!-- FastClick -->
<script src="<?php echo e(url('public/plugins/fastclick/fastclick.js')); ?>"></script>
<!-- ChartJs -->
<script src="<?php echo e(url('public/plugins/chartjs/Chart.min.js')); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo e(url('public/dist/js/app.min.js')); ?>"></script>
<!-- Ckeditor -->
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>

<script>
$('.update_password').click(function(e)
{
    e.preventDefault();
    var old_password = $('.old_pass').val();
    var new_password = $('.new_pass').val();

    $.ajax(
    {
        type: 'GET',
        url: '<?php echo e(url('dealer/changepassword')); ?>',
        data: {old_pass: old_password , new_pass: new_password},
        success: function (response)
        {
            if($.trim(response)=="success")
            {
                $(".error_message").html("<p style='color:green'>Password updated Successfully</p>");
            }
            else
            {
                $(".error_message").html("<p style='color:red'>"+response+"</p>");
            }

        }

    });

});

var source = new EventSource("<?php echo e(url('dealer/notification')); ?>");
source.onmessage = function(e)
{
    var data=JSON.parse(e.data);
    if((data['order']>0))
    {
        $(".notification").html("1");
    }
    else
    {
        $(".notification").html("");
    }
    if(data['order']>0)
    {
        $(".order").css("display", "block");
        $(".order").html("<a href='<?php echo e(url('dealer/receive_stock')); ?>'><i class='fa fa-truck text-aqua'></i>"+data['order']+" product shipped by Merchant </a>");
    }
    else
    {
        $(".order").css("display", "none");
    }

}
</script>
<?php echo $__env->yieldContent('custom_script'); ?>
</body>
</html>
