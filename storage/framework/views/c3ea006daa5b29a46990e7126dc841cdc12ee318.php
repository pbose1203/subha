<?php $title = "Product" ?>

<?php $__env->startSection('content'); ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Product
            <small>(Add new Item)</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo e(url('superadmin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><a href="<?php echo e(url('superadmin/product')); ?>">Item</a></li>
            <li class="active">Add Item</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- /.row -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add new Product</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <?php if(Session::has('errors')): ?>
                        <span class="help-block" style="color:red;margin-left: 10px;">
                            <?php if($errors->has('category')): ?>
                                <?php echo e($errors->first('category')); ?>

                            <?php elseif($errors->has('name')): ?>
                                <?php echo e($errors->first('name')); ?>

                            <?php elseif($errors->has('price')): ?>
                                <?php echo e($errors->first('price')); ?>

                            <?php elseif($errors->has('description')): ?>
                                <?php echo e($errors->first('description')); ?>

                            <?php endif; ?>
                        </span>
                    <?php endif; ?>
                    <form role="form" action="<?php echo e(url('superadmin/product')); ?>" method="post" enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="category_name">Select Category</label>
                                <select class="form-control category" name="category">
                                        <option selected="selected" value="">Category</option>
                                    <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($category->id); ?>"><?php echo e($category->name); ?></option>
                                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="product_name">Enter Item Name</label>
                                <input type="text" class="form-control" id="product_name" placeholder="Enter the Item Name" name="product_name">
                            </div>
                            <div class="form-group">
                                <label for="product_name">Enter Item Display Name</label>
                                <input type="text" class="form-control" id="product_name" placeholder="Enter the Item Display Name" name="display_product_name">
                            </div>
                            <div class="form-group">
                                <label for="product_name">Enter Unit</label>
                                <input type="text" class="form-control" id="product_name" placeholder="Enter the Item Display Name" name="unit">
                            </div>
                            <div class="form-group">
                                <label for="product_name">Preparation Time</label>
                                <input type="text" class="form-control" id="product_name" placeholder="Enter Preparation Time in Minutes" name="unit">
                            </div>
                            <div class="form-group">
                                <label for="product_cost_price">Enter Product Cost Price</label>
                                <input type="text" class="form-control" id="product_cost_price" placeholder="Enter the Product Cost Price" name="product_cost_price">
                            </div>
                            <div class="form-group">
                                <label for="product_MRP">Enter Product MRP</label>
                                <input type="text" class="form-control" id="product_MRP" placeholder="Enter the Product MRP" name="product_MRP">
                            </div>

                            <div class="form-group">
                                <label for="product_description">Enter Product Description</label>
                                <textarea id="product_description"  rows="10" cols="80" name="product_description"></textarea>
                            </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom_script'); ?>
<script>
    $(document).ready(function()
    {
        CKEDITOR.replace('product_description');
    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('superadmin.masterlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>