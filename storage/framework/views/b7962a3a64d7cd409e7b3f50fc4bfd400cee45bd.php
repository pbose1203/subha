
<?php $title = "Customer" ?>

<?php $__env->startSection('content'); ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Customer
            <small>(View, Add, Edit dealer)</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo e(url('employee/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Customer</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- /.row -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add new Customer</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <?php if(Session::has('errors')): ?>
                        <span class="help-block" style="color:red;margin-left: 10px;">
                            <?php if($errors->has('category')): ?>
                                <?php echo e($errors->first('category')); ?>

                            <?php elseif($errors->has('image')): ?>
                                <?php echo e($errors->first('image')); ?>

                            <?php endif; ?>
                        </span>
                    <?php endif; ?>
                    <form class="check-mobile-form" method="post" action="<?php echo e(url('employee/check_customer')); ?>">
                        <?php echo csrf_field(); ?>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="category">Enter Phone No:</label>
                                <input type="text" class="form-control mobile" id="category" placeholder="Enter phone no" name="phone_no" >
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button class="btn btn-primary check-mobile-btn" data-target="#verify_code">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
            <!--  modal open -->
            

            <!-- Message Modal -->
            
            <!-- modal close -->
        </div>
        <!-- /.row -->
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom_script'); ?>



<?php $__env->stopSection(); ?>



<?php echo $__env->make('employee.masterlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>