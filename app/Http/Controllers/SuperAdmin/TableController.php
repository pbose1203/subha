<?php

namespace App\Http\Controllers\Superadmin;

use App\Pos;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Mail;
use App\Table;

class TableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tables = Table::get();
        return view('superadmin.table', compact('tables'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('superadmin.addTable');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $check = Table::where('table_no','=',$request->name)->first();
        if(count($check) > 0)
        {
            //dd(count($check));
            return redirect('superadmin/table')->with('error', 'Table number '.$request->name.' already present');

        }
        else{
            //dd('hello');
            $table = new Table();
            $table->table_no = $request->name;
            $table->active = 0;
            $table->status = 1;
            $table->save();
            echo "success";
            return redirect('superadmin/table')->with('success', 'Table added successfully');

        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pos = Pos::where('id', $id)->first();
        return view('superadmin.editPos', compact('pos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $name = $request->name;
        $display_name = $request->display_name;

        Pos::where('id', $id)->update(['name' => $name , 'display_name' => $display_name ]);
        return redirect('superadmin/pos')->with('success', 'POS updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
