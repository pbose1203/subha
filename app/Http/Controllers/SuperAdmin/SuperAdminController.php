<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Http\Request;
use App\User;


class SuperAdminController extends Controller
{

    //Login functionality for SuperAdmin
    public function login()
    {
        if ((Auth::check())&&(Auth::user()->type=='superadmin'))
        {
            return redirect('superadmin/dashboard');
        }
        else
        {
            return view('superadmin.login');
        }
    }

    //Logout functionality for SuperAdmin
    public function logout()
    {
        Auth::logout();
        return redirect('superadmin');
    }

    //Authenticate whether an authorized SuperAdmin or a hacker
    public function authenticate(Request $request)
    {
        $validator = Validator::make(
            array(
                'email' => $request->email,
                'password' => $request->password
            ),array(
                'email' => 'required|email',
                'password' => 'required',

            )
        );
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        else
        {
            $details = array('email' => $request->email, 'password' => $request->password , 'type' =>'superadmin');
            if (Auth::attempt($details))
            {
                //dd("hello1");
                return redirect('superadmin/dashboard');
            }
            else
            {
                //dd("hello");
                return redirect()->back()->with('error','Invalid Email or Password');
            }
        }
    }

    // Access to the Dashboard for an authorized SuperAdmin
    public function dashboard()
    {

        return view('superadmin.dashboard');

    }

    // Change Password
    public function updatePassword(Request $request)
    {
        $id = Auth::user()->id;

        $old_password = $request->old_pass;
        $new_password = bcrypt($request->new_pass);

        $user = new User;
        $stored_password = $user->where('id',$id)->value('password');

        if(password_verify($old_password , $stored_password ))
        {
            $user = $user->findOrFail($id);
            $user->password = $new_password;
            $user->save();
            Auth::logout();
            echo "success";
        }
        else
        {
            echo "The old password does not match with our database";
        }

    }

    //notification related to  new order request
    public function notifications()
    {
        $orders = Order::count();

        header('Content-Type: text/event-stream');
        header('Cache-Control: no-cache');

        $data = array("order"=>$orders);

        echo 'data: '. json_encode($data)."\n\n";
        ob_flush();
        flush();

    }
}
