<?php

namespace App\Http\Controllers\Superadmin;

use App\Pos;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Mail;

class PosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $poss = Pos::get();
        return view('superadmin.pos', compact('poss'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('superadmin.addPos');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pos = new Pos;
        $pos->name = $request->name;
        $pos->display_name = $request->display_name;
        $pos->status = 1;
        $pos->save();
        echo "success";
        return redirect('superadmin/pos')->with('success', 'POS added successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pos = Pos::where('id', $id)->first();
        return view('superadmin.editPos', compact('pos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $name = $request->name;
        $display_name = $request->display_name;

        Pos::where('id', $id)->update(['name' => $name , 'display_name' => $display_name ]);
        return redirect('superadmin/pos')->with('success', 'POS updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
