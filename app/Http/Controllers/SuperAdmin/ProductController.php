<?php

namespace App\Http\Controllers\SuperAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Category;
use App\Product;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $products = Product::get();
        $category_name = array();
        
        foreach($products as $product)
        {
            $category_name[] = Category::where('id',$product->category_id)->value('name');
        }
        return view('superadmin.product', compact('products','category_name'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $category = new Category;
        $categories = $category->get();

        return view('superadmin.addProduct',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $product = new Product;
        
        $validator = Validator::make(
            array
            (
                'category' => $request->category,
                'name' => $request->product_name,
                'cost price' => $request->product_cost_price,
                'MRP' => $request->product_MRP,
                'description' => $request->product_description,
            ),
            array
            (
                'category' => 'required',
                'name' => 'required|unique:products,name',
                'cost price' => 'required',
                'MRP' => 'required',
                'description' => 'required',
            )
        );
        if ($validator->fails())
        {
            return redirect('superadmin/product/create')->withErrors($validator)->withInput();
        }
        else
        {


            $product->category_id = $request->category;
            $product->name = $request->product_name;
            $product->cost_price = $request->product_cost_price;
            $product->MRP = $request->product_MRP;
            $product->description = $request->product_description;
            $product->display_product_name = $request->display_product_name;
            $product->unit = $request->unit;
            $product->preparation_time = $request->preparation_time;
            $product->save();
            return redirect('superadmin/product')->with('success', 'Product added successfully');
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $category = new Category;
        $categories = $category->get();

        $product = Product::where('id',$id)->first();
        return view('superadmin.editProduct', compact('categories','product'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
            $validator = Validator::make(
                array
                (
                    'category' => $request->category,
                    'name' => $request->product_name,
                    'cost price' => $request->product_cost_price,
                    'MRP' => $request->product_MRP,
                    'description' => $request->product_description,
                ),
                array
                (
                    'category' => 'required',
                    'name' => 'required',
                    'cost price' => 'required',
                    'MRP' => 'required',
                    'description' => 'required',
                )
            );
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        else
        {
            $product = Product::findOrFail($id);

                $product->category_id = $request->category;
                $product->name = $request->product_name;
                $product->cost_price = $request->product_cost_price;
                $product->MRP = $request->product_MRP;
                $product->description = $request->product_description;
                $product->save();

            return redirect('superadmin/product')->with('success', 'Product updated successfully');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        //
    }
}
