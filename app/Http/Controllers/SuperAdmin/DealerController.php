<?php

namespace App\Http\Controllers\SuperAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Mail;

class DealerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dealers = User::where('type','dealer')->get();
        return view('superadmin.dealer', compact('dealers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('superadmin.addDealer');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User;

        //$password = str_random(20);
        $password = 'tpl_demo';
        $database_password = bcrypt($password);

        $user->name = $request->dealer_name;
        $user->email = $request->dealer_email;
        $user->phone = $request->dealer_phone;
        $user->password = $database_password;
        $user->address = $request->dealer_address;
        $user->type = "dealer";
        $user->ps = $password;
        $user->save();
        
        $userid = $user->id;

        $user = User::findOrFail($userid);
        
        /*Mail::send('mail', ['password' => $password], function($message) use($user)
        {
            $message->from('tpl@gmail.com','TPL');
            $message->to($user->email, $user->name)->subject('New Registration message from TPL');
        });*/
        echo "success";
        return redirect('superadmin/dealer')->with('success', 'Admin added successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dealer = User::where('id', $id)->first();
        return view('superadmin.editDealer', compact('dealer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dealer_name = $request->dealer_name;
        $dealer_email = $request->dealer_email;
        $dealer_phone = $request->dealer_phone;
        $dealer_address = $request->dealer_address;

        User::where('id', $id)->update(['name' => $dealer_name , 'email' => $dealer_email , 'phone' =>$dealer_phone , 'address' =>$dealer_address ]);
        return redirect('superadmin/dealer')->with('success', 'Admin updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
