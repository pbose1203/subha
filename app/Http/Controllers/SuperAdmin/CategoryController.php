<?php

namespace App\Http\Controllers\SuperAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Category;
use App\Tax;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::leftJoin('taxs', function($join){
            $join->on('taxs.id', '=', 'categories.tax_id');
        })->select('categories.id','categories.name','categories.image','categories.tag','taxs.percentage')->get();
        //dd($categories);
        return view('superadmin.category',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$taxs = Tax::get();
        return view('superadmin.addCategory',compact('taxs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = new Category;
        
        $validator = Validator::make(
            array
            (
                'category' => $request->category_name,
                'image' => $request->file('image'),
            ),
            array
            (
                'category' =>'required|unique:categories,name',
                'image' => 'required|max:1000|mimes:jpeg,jpg,png', //maximum size in kb
            )
        );
        if($validator->fails())
        {
            return redirect('superadmin/category/create')->withErrors($validator)->withInput();
        }
        else
        {

            $image = $request->file('image');
            $image_name = time().$image->getClientOriginalName();
            $image->move('public/upload', $image_name);

            $category->name = $request->category_name;
            $category->image = $image_name;
            $category->tag = $request->category_tag;
            $category->save();
            return redirect('superadmin/category')->with('success', 'Category added successfully');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::where('id',$id)->first();
        return view('superadmin.editCategory',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = new Category;
        $category = $category->findOrFail($id);

        $image = $request->file('image');
        if($image)
        {
            $image_name = time().$image->getClientOriginalName();
            $image->move('public/upload', $image_name);
            $category->name = $request->category_name;
            $category->image = $image_name;
            $category->tag = $request->category_tag;
            $category->save();
        }
        else
        {
            $category->name = $request->category_name;
            $category->tag = $request->category_tag;
            $category->save();
        }


        return redirect('superadmin/category')->with('success','Category updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
