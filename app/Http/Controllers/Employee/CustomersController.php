<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
//use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Customer;
use Illuminate\Support\Facades\Redirect;



class CustomersController extends Controller
{



    public function checkCustomer(Request $request)
    {
        $mobile = $request->phone_no;
        //echo $mobile = $request['phone_no'];
        $customer = Customer::where('mobile', $mobile)->first();

        //echo count($customer_id);

        if($customer)
        {
            //echo $customer;

            return Redirect::to('employee/customer/'.$customer->id);
            //echo json_encode($customer);
        }
        else
        {
            $ck = new Customer;
            $ck->mobile = $mobile;
            $ck->save();
            $customer = Customer::where('mobile', $mobile)->first();
            return Redirect::to('employee/customer/'.$customer->id);
            //echo json_encode($customer);
            //$customer_id = Customer::where('mobile', $mobile)->first();
            //echo "All Correct";
        }
        //echo "All Correct";
    }

    public function singleCustomer($id)
    {
        //$mobile = $request->phone_no;
        //echo $mobile = $request['phone_no'];
        $customer = Customer::where('id', $id)->first();

        //echo count($customer_id);

        if($customer)
        {


            return view('employee.singleCustomer', compact('customer'));

        }
        else
        {

        }
        //echo "All Correct";
    }

    public function editCustomer($id)
    {
        //$mobile = $request->phone_no;
        //echo $mobile = $request['phone_no'];
        $customer = Customer::where('id', $id)->first();

        //echo count($customer_id);

        if($customer)
        {


            return view('employee.editCustomer', compact('customer'));

        }
        else
        {

        }
        //echo "All Correct";
    }

    public function addMoney(Request $request)
    {
        $id = $request->id;
        //echo $mobile = $request['phone_no'];
        $customer1 = Customer::where('id', $id)->first();

        //echo count($customer_id);

        if($customer1)
        {
            $customer1->balance = $customer1->balance + $request->balance;
            $customer1->save();
            $customer = Customer::where('id', $id)->first();

            return view('employee.singleCustomer', compact('customer'));

        }
        else
        {

        }
        //echo "All Correct";
    }

    public function editCustomerSave(Request $request)
    {
        $id = $request->id;
        //echo $mobile = $request['phone_no'];
        $customer1 = Customer::where('id', $id)->first();

        //echo count($customer_id);

        if($customer1)
        {
            $customer1->name = $request->name;
            $customer1->save();
            $customer = Customer::where('id', $id)->first();

            return view('employee.singleCustomer', compact('customer'));

        }
        else
        {

        }
        //echo "All Correct";
    }
}
