<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Customer;


class EmployeeController extends Controller
{

    //Login functionality for Dealer
    public function login()
    {
        if ((Auth::check())&&(Auth::user()->type=='employee'))
        {

            return redirect('employee/dashboard');
        }
        else
        {
            return view('employee.login');
        }
    }

    //Logout functionality for Dealer
    public function logout()
    {
        Auth::logout();
        return redirect('employee');
    }

    //Authenticate whether an authorized Dealer or a hacker
    public function authenticate(Request $request)
    {
        $validator = Validator::make(
            array(
                'email' => $request->email,
                'password' => $request->password
            ),array(
                'email' => 'required|email',
                'password' => 'required',

            )
        );
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        else 
        {
            $details = array('email' => $request->email, 'password' => $request->password, 'type' =>'employee');
            if (Auth::attempt($details))
            {

                return redirect('employee/dashboard');
            }
            else
            {
                return redirect()->back()->with('error','Invalid Email or Password');
            }
        }
    }

    // Access to the Dashboard for an authorized Dealer
    public function dashboard()
    {
            return view('employee.dashboard');

    }

    // Change Password
    public function updatePassword(Request $request)
    {
        $id = Auth::user()->id;

        $old_password = $request->old_pass;
        $new_password = bcrypt($request->new_pass);

        $user = new User;
        $stored_password = $user->where('id',$id)->value('password');
        
        if(password_verify($old_password , $stored_password ))
        {
            $user = $user->findOrFail($id);
            $user->password = $new_password;
            $user->save();
            Auth::logout();
            echo "success";
        }
        else
        {
            echo "The old password does not match with our database";
        }

    }

    //notification related to order shipped by merchant
    public function notifications()
    {
        $orders = Stock::where('stock_with', Auth::user()->id)->where('status','Order shipped')->count();

        header('Content-Type: text/event-stream');
        header('Cache-Control: no-cache');

        $data = array("order"=>$orders);

        echo 'data: '. json_encode($data)."\n\n";
        ob_flush();
        flush();

    }

    public function customer()
    {
        return view('employee.customer');
    }

    public function checkCustomer(Request $request)
    {
        /*$mobile = $request->mobile_no;
        $customer_id = Customer::where('mobile', $mobile)->value('id');*/

        /*if($customer_id)
        {
            echo "available";
        }
        else
        {
            echo "not available";
        }*/
        echo "available";
    }
}
