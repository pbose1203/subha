<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Mail;

class PasswordResetController extends Controller
{
    // Send password reset link
    public function forgotPassword(Request $request)
    {
        $email = $request->email;
        $mobile = $request->mobile;

        $id = User::where('email', $email)->where('phone', $mobile)->value('id');

        if ($id != "")
        {
            $new_password = str_random(20);
            //$new_password = "tpl_admin";
            $database_password = bcrypt($new_password);

            User::where('id', $id)->update(['password' => $database_password]);
            $user = User::findOrFail($id);

            Mail::send('mail', ['password' => $new_password], function($message) use($user)
            {
                $message->from('tpl@gmail.com','TPL');
                $message->to($user->email, $user->name)->subject('Password change message from TPL');
            });
            echo "success";
        }
        else
        {
            echo "The email or mobile does not match with our database";
        }

    }


}
