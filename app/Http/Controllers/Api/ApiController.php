<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use App\Category;
use App\Product;
use App\Department;
use Validator;
use Illuminate\Support\Facades\Hash;
use Response;

class ApiController extends Controller
{
    public function category(Request $request)
    {
        
        $categories = Category::get();

        //$products = Product::get();

        return json_encode(array('categories'=>$categories));
       
    }
    public function products(Request $request)
    {

        $categories = Category::get();

        $products = Product::get();

        return json_encode(array('products'=>$products));

    }
    public function slide(Request $request)
    {


        $products = Product::take(5)->get();

        return json_encode(array('products'=>$products));

    }
    public function login(Request $request)
    {
        $validator = Validator::make(
            array
            (
                'email' => $request->email,
                'password' => $request->password,
            ),
            array
            (
                'email' => 'required',
                'password' => 'required',
            )
        );
        if ($validator->fails())
        {
            return Response::json(array(
                    'error' => true,
                    'response' => 'Please fill all required field',
                    'users' => [],
                    'product' => [],
                    'department' => [],
                    'category' => [],
                )
            );
        }
        else
        {
            $usercheck = User::where('email','=',$request->email)->first();
            if(count($usercheck) > 0 )
            {
                //$checkPassword=Hash::check($request->password,$findUser->password);
                if (Hash::check($request->password, $usercheck->password)) {

                    $users= User::where('id','=',$usercheck->id)->select('name','id','email','phone','address','type')->get();
                    $category= Category::where('status','=','1')->get();
                    $product = Product::where('status','=','1')->get();
                    $department = Department::where('status','=','1')->get();
                    return Response::json(array(
                            'error' => false,
                            'response' => 'Sucesses',
                            'users' => $users,
                            'product' => $product,
                            'department' => $department,
                            'category' => $category,
                        )
                    );
                }
                else
                {
                    $users = new User();
                    return Response::json(array(
                            'error' => true,
                            'response' => 'Incorrect Password',
                            'users' => [],
                            'product' => [],
                            'department' => [],
                            'category' => [],
                        )
                    );
                }
            }
            else
            {
                return Response::json(array(
                        'error' => true,
                        'response' => 'Please fill all required field',
                        'users' => [],
                        'product' => [],
                        'department' => [],
                        'category' => [],
                        )
                );
            }

        }

    }
    public function checkEmp(Request $request)
    {
        $validator = Validator::make(
            array
            (
                'id' => $request->id,
                'type' => $request->type,
            ),
            array
            (
                'id' => 'required',
                'type' => 'required',
            )
        );
        if ($validator->fails())
        {
            return Response::json(array(
                    'error' => true,
                    'response' => 'Please fill all required field',
                    'users' => [],
                    'product' => [],
                    'department' => [],
                    'category' => [],
                )
            );
        }
        else
        {
            $usercheck = User::where('id','=',$request->id)->where('type','=',$request->type)->first();
            if(count($usercheck) > 0 )
            {
                    $users= User::where('id','=',$usercheck->id)->select('name','id','email','phone','address','type')->get();
                    $category= Category::where('status','=','1')->get();
                    $product = Product::where('status','=','1')->get();
                    $department = Department::where('status','=','1')->get();
                    return Response::json(array(
                            'error' => false,
                            'response' => 'Sucesses',
                            'users' => $users,
                            'product' => $product,
                            'department' => $department,
                            'category' => $category,
                        )
                    );

            }
            else
            {
                return Response::json(array(
                        'error' => true,
                        'response' => 'Please fill all required field',
                        'users' => [],
                        'product' => [],
                        'department' => [],
                        'category' => [],
                    )
                );
            }

        }

    }

}
