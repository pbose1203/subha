<?php

namespace App\Http\Controllers\Dealer;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use App\User;



class DealerController extends Controller
{

    //Login functionality for Dealer
    public function login()
    {
        if ((Auth::check())&&(Auth::user()->type=='dealer'))
        {
            return redirect('dealer/dashboard');
        }
        else
        {
            return view('dealer.login');
        }
    }

    //Logout functionality for Dealer
    public function logout()
    {
        Auth::logout();
        return redirect('dealer');
    }

    //Authenticate whether an authorized Dealer or a hacker
    public function authenticate(Request $request)
    {
        $validator = Validator::make(
            array(
                'email' => $request->email,
                'password' => $request->password
            ),array(
                'email' => 'required|email',
                'password' => 'required',

            )
        );
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        else 
        {
            $details = array('email' => $request->email, 'password' => $request->password, 'type' =>'dealer');
            if (Auth::attempt($details))
            {
                return redirect('dealer/dashboard');
            }
            else
            {
                return redirect()->back()->with('error','Invalid Email or Password');
            }
        }
    }

    // Access to the Dashboard for an authorized Dealer
    public function dashboard()
    {

            /*$months = array('January','February','March','April','May','June','July','August','September','October','November','December');
            $line_graph = array('January'=>0,'February'=>0,'March'=>0,'April'=>0,'May'=>0,'June'=>0,'July'=>0,'August'=>0,'September'=>0,'October'=>0,'November'=>0,'December'=>0);

            $sold_stock = DealerSoldStock::where('transaction_id',DealerTransaction::where('dealer_id', Auth::user()->id)->value('id'))->get();
            foreach($sold_stock as $sold)
            {
                $month = date('F',strtotime($sold->created_at));
                if (in_array($month, $months))
                {
                    $line_graph[$month] = $line_graph[$month]+1;
                }
            }*/

            return view('dealer.dashboard');



    }

    // Change Password
    public function updatePassword(Request $request)
    {
        $id = Auth::user()->id;

        $old_password = $request->old_pass;
        $new_password = bcrypt($request->new_pass);

        $user = new User;
        $stored_password = $user->where('id',$id)->value('password');
        
        if(password_verify($old_password , $stored_password ))
        {
            $user = $user->findOrFail($id);
            $user->password = $new_password;
            $user->save();
            Auth::logout();
            echo "success";
        }
        else
        {
            echo "The old password does not match with our database";
        }

    }

    //notification related to order shipped by merchant
    public function notifications()
    {
        $orders = Stock::where('stock_with', Auth::user()->id)->where('status','Order shipped')->count();

        header('Content-Type: text/event-stream');
        header('Cache-Control: no-cache');

        $data = array("order"=>$orders);

        echo 'data: '. json_encode($data)."\n\n";
        ob_flush();
        flush();

    }
}
