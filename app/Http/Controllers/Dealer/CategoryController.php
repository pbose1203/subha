<?php

namespace App\Http\Controllers\Dealer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::get();
        return view('dealer.category',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dealer.addCategory');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = new Category;
        
        $validator = Validator::make(
            array
            (
                'category' => $request->category_name,
                'image' => $request->file('image'),
            ),
            array
            (
                'category' =>'required|unique:categories,name',
                'image' => 'required|max:1000|mimes:jpeg,jpg,png', //maximum size in kb
            )
        );
        if($validator->fails())
        {
            return redirect('dealer/category/create')->withErrors($validator)->withInput();
        }
        else
        {

            $image = $request->file('image');
            $image_name = time().$image->getClientOriginalName();
            $image->move('public/upload', $image_name);

            $category->name = $request->category_name;
            $category->image = $image_name;
            $category->tag = $request->category_tag;
            $category->save();
            return redirect('dealer/category')->with('success', 'Category added successfully');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::where('id',$id)->first();
        return view('dealer.editCategory',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = new Category;
        $category = $category->findOrFail($id);

        $image = $request->file('image');
        if($image)
        {
            $image_name = time().$image->getClientOriginalName();
            $image->move('public/upload', $image_name);
            $category->name = $request->category_name;
            $category->image = $image_name;
            $category->tag = $request->category_tag;
            $category->save();
        }
        else
        {
            $category->name = $request->category_name;
            $category->tag = $request->category_tag;
            $category->save();
        }


        return redirect('dealer/category')->with('success','Category updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
