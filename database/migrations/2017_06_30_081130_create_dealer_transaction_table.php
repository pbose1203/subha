<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealerTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dealer_transaction', function (Blueprint $table) {
            $table->increments('id');
            $table->string('invoice_no');
            $table->string('dealer_id');
            $table->string('customer_id');
            $table->string('quantity');
            $table->string('total_amount');
            $table->string('payment_no');
            $table->string('payment_mode');
            $table->string('payment_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dealer_transaction');
    }
}
