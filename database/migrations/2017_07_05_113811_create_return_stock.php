<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReturnStock extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('return_stock', function (Blueprint $table) {
            $table->increments('id');
            $table->string('transaction_id');
            $table->string('stock_id');
            $table->string('return_type');
            $table->string('return_amount');
            $table->string('return_reason');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('return_stock');
    }
}
