<?php

Route::GET('get_category', 'Api\ApiController@category');
Route::GET('get_products', 'Api\ApiController@products');
Route::GET('get_slide', 'Api\ApiController@slide');
Route::POST('login', 'Api\ApiController@login');
Route::prefix('/v1')->group(function ()
{
    Route::POST('checkEmp', 'Api\ApiController@checkEmp');
});
