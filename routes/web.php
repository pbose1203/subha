<?php
Route::GET('forgotpassword', 'PasswordResetController@forgotPassword');

Route::group(['prefix' => 'superadmin', 'namespace' => 'SuperAdmin'], function()
{
    Route::GET('/', 'SuperAdminController@login');

    Route::GET('login', 'SuperAdminController@login');

    Route::POST('authenticate', 'SuperAdminController@authenticate');
    
});

Route::group(['prefix' => 'superadmin', 'namespace' => 'SuperAdmin','middleware' => 'superadmin'], function()
{

    Route::GET('logout', 'SuperAdminController@logout');

    Route::GET('dashboard', 'SuperAdminController@dashboard');

    Route::GET('changepassword', 'SuperAdminController@updatePassword');

    Route::Resource('master', 'DealerController');

    Route::Resource('category', 'CategoryController');

    Route::Resource('product', 'ProductController');

    Route::Resource('employee', 'EmployeeController');

    Route::Resource('pos', 'PosController');

    Route::Resource('waiter', 'WaiterController');

    Route::Resource('table', 'TableController');

    Route::GET('notification', 'SuperAdminController@notifications');

});

Route::group(['prefix' => 'dealer', 'namespace' => 'Dealer'], function()
{
    Route::GET('/' , 'DealerController@login');

    Route::GET('login' , 'DealerController@login');

    Route::POST('authenticate' , 'DealerController@authenticate');
    
});

Route::group(['prefix' => 'dealer', 'namespace' => 'Dealer','middleware' => 'dealer'], function()
{

    Route::GET('logout' , 'DealerController@logout');

    Route::GET('changepassword', 'DealerController@updatePassword');

    Route::GET('dashboard' , 'DealerController@dashboard');

    Route::GET('notification', 'DealerController@notifications');

    Route::Resource('category', 'CategoryController');

    Route::Resource('product', 'ProductController');

    Route::Resource('employee', 'EmployeeController');

});

Route::GET('superadmin/sanjay', 'SuperAdmin\StockController@sanjay');

Route::group(['prefix' => 'employee', 'namespace' => 'Employee'], function()
{
    Route::GET('/' , 'EmployeeController@login');

    Route::GET('login' , 'EmployeeController@login');

    Route::POST('authenticate' , 'EmployeeController@authenticate');


});

Route::group(['prefix' => 'employee', 'namespace' => 'Employee','middleware' => 'employee'], function()
{

    Route::GET('logout' , 'EmployeeController@logout');

    Route::GET('changepassword', 'EmployeeController@updatePassword');

    Route::GET('dashboard' , 'EmployeeController@dashboard');

    Route::GET('customer' , 'EmployeeController@customer');

    Route::POST('check_customer', 'CustomersController@checkCustomer');

    Route::GET('customer/{id}' , 'CustomersController@singleCustomer');

    Route::GET('customer/edit/{id}' , 'CustomersController@editCustomer');

    Route::POST('customer/edit' , 'CustomersController@editCustomerSave');

    Route::POST('add_money/' , 'CustomersController@addMoney');

    Route::GET('notification', 'EmployeeController@notifications');



});
